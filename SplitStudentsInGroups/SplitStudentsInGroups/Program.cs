﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitStudentsInGroups
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> list = CreateList(17);
            Print(list);
            Console.WriteLine();
            List<List<Student>> groupList = SplitInGroups(list, 3);
            Print(groupList);

            Console.ReadKey();
        }
        static List<Student> CreateList(int count)
        {
            List<Student> list = new List<Student>(count);
            for (int i = 0; i < count; i++)
            {
                list.Add(new Student(
                    $"A{i + 1}",
                    $"A{i + 1}yan", $"A{i + 1}@gmail.com",
                    i + 1));
            }
            return list;
        }
        static void Print(List<Student> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine($"{list[i].Name}, {list[i].Surnmae}, {list[i].Email}, {list[i].Age}");
            }
        }
        static List<List<Student>> SplitInGroups(List<Student> list, int groupCount)
        {
            List<List<Student>> groupList = new List<List<Student>>(groupCount);
            if (groupCount <= 1)
            {
                groupList.Add(list);
                return groupList;
            }
            else if (list.Count < groupCount)
            {
                groupList.Add(list);
                return groupList;
            }
            else
            {
                while (groupCount > 0)
                {
                    int count = list.Count / groupCount;
                    groupList.Add(MoveToGroup(list, count));
                    groupCount--;
                }
                return groupList;
            }
        }
        static List<Student> MoveToGroup(List<Student> list, int groupCount)
        {
            List<Student> studentGroup = new List<Student>(groupCount);
            for (int i = 0; i < groupCount; i++)
            {
                studentGroup.Add(list[0]);
                list.RemoveAt(0);
            }
            return studentGroup;
        }
        static void Print(List<List<Student>> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < list[i].Count; j++)
                {
                    Console.WriteLine($"{list[i][j].Name}, {list[i][j].Surnmae}, {list[i][j].Email}, {list[i][j].Age}");
                }
                Console.WriteLine();
            }
        }
    }
}
